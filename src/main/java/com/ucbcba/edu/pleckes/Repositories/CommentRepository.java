package com.ucbcba.edu.pleckes.Repositories;

import com.ucbcba.edu.pleckes.Entities.Comment;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface CommentRepository extends CrudRepository<Comment,Integer> {
}
