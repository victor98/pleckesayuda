package com.ucbcba.edu.pleckes.Repositories;

import com.ucbcba.edu.pleckes.Entities.Post;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

@Transactional
public interface PostRepository extends CrudRepository<Post,Integer> {
    @Query("select p from Post p where p.text like CONCAT('%',:text,'%')")
    public Iterable<Post>findByTitle(@Param("text") String text);
}
