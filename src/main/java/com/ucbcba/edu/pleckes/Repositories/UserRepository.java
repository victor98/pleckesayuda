package com.ucbcba.edu.pleckes.Repositories;

import com.ucbcba.edu.pleckes.Entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface UserRepository extends JpaRepository<User,Integer> {
    User findByUsername(String username);
}
