package com.ucbcba.edu.pleckes.Repositories;

import com.ucbcba.edu.pleckes.Entities.Category;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface CategoryRepository extends CrudRepository<Category,Integer> {
}