package com.ucbcba.edu.pleckes.Services;

import com.ucbcba.edu.pleckes.Entities.Comment;
import com.ucbcba.edu.pleckes.Repositories.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CommentServiceImpl implements CommentService {
    CommentRepository commentRepository;
    @Override
    public Iterable<Comment> listAllComments() {
        return commentRepository.findAll();
    }

    @Override
    public void save(Comment comment) {
        commentRepository.save(comment);
    }

    @Override
    public void deleteCommentById(Integer id) {
        commentRepository.deleteById(id);
    }

    @Override
    public Comment findComment(Integer id) {
        Optional<Comment>opt;
        opt=commentRepository.findById(id);
        return opt.get();
    }

    @Autowired
    @Qualifier(value = "commentRepository")
    public void setCommentRepository(CommentRepository commentRepository){this.commentRepository=commentRepository;}

}
