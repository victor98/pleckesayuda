package com.ucbcba.edu.pleckes.Services;


import com.ucbcba.edu.pleckes.Entities.Comment;
import com.ucbcba.edu.pleckes.Entities.Post;

public interface PostService  {
    Iterable<Post>listAllPosts();
    void save (Post post);
    void deleteById (Integer id);
    Post findPost (Integer id);
    Iterable<Post>findByTitle(String query);
}
