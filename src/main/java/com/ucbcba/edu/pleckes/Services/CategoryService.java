package com.ucbcba.edu.pleckes.Services;

import com.ucbcba.edu.pleckes.Entities.Category;

public interface CategoryService {

    Iterable<Category> listAllCategories();
    Category findCategory(Integer id);
    void saveCategory(Category category);
    void deleteCategory(Integer id);

}