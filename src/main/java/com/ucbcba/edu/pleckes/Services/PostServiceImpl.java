package com.ucbcba.edu.pleckes.Services;

import com.ucbcba.edu.pleckes.Entities.Comment;
import com.ucbcba.edu.pleckes.Entities.Post;
import com.ucbcba.edu.pleckes.Repositories.PostRepository;
import javafx.geometry.Pos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array.*;
import java.util.*;


@Service
public class PostServiceImpl implements PostService {
    private PostRepository postRepository;


    @Autowired
    @Qualifier(value="postRepository")
    public void setPostRepository(PostRepository postRepository){
        this.postRepository=postRepository;
    }

    public Post comparar(Post l1, Post l2){
        if (l1.getLikes()>l2.getLikes())
            return l1;
        else
            return l2;
    }
    @Override
    public Iterable<Post> listAllPosts() {
        /*List<Post> list = (ArrayList)postRepository.findAll();
        for (int i = 0; i < list.size()-1; i++)
            for (int j = 0; j < list.size()-i-1; j++){
                if (list.get(j).getLikes() > list.get(j+1).getLikes()){
                    swap(list.get(j), list.get(j+1));
                }
            }
        return (Iterable) list;*/
        return postRepository.findAll();

    }

    /*private void swap(Post post, Post post1) {
        Post temp = post;
        post = post1;
        post1 = temp;
    }*/

    @Override
    public void save(Post post) {
        postRepository.save(post);
    }

    @Override
    public void deleteById(Integer id) {
        postRepository.deleteById(id);
    }

    @Override
    public Post findPost(Integer id) {
        Optional<Post> opt;
        opt=postRepository.findById(id);
        return  opt.get();
    }
    @Override
    public Iterable<Post> findByTitle(String query){return postRepository.findByTitle(query);}

}
