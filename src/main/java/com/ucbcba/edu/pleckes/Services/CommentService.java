package com.ucbcba.edu.pleckes.Services;
import com.ucbcba.edu.pleckes.Entities.Comment;
public interface CommentService {
        Iterable<Comment>listAllComments();
        void save (Comment comment);
        void deleteCommentById(Integer id);
        Comment findComment(Integer id);

}
