package com.ucbcba.edu.pleckes.Services;
import com.ucbcba.edu.pleckes.Entities.User;
public interface UserService {
    void save(User user);
    User findByUsername(String username);
    User findById(Integer id);
}
