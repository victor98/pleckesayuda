package com.ucbcba.edu.pleckes.Services;

import com.ucbcba.edu.pleckes.Entities.Category;
import com.ucbcba.edu.pleckes.Repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService{

    CategoryRepository categoryRepository;

    @Autowired
    @Qualifier(value = "categoryRepository")
    public void setCategoryRepository(CategoryRepository categoryRepository){
        this.categoryRepository = categoryRepository;
    }


    @Override
    public Iterable<Category> listAllCategories() {
        return categoryRepository.findAll();
    }

    public Category findCategory(Integer id) {
        Optional<Category> opt;
        opt=categoryRepository.findById(id);
        return opt.get();
    }

    @Override
    public void saveCategory(Category category) {
        categoryRepository.save(category);
    }

    @Override
    public void deleteCategory(Integer id) {
        categoryRepository.deleteById(id);
    }


}