package com.ucbcba.edu.pleckes.Entities;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.validation.constraints.Null;
import java.util.Set;

@Entity
@Table(name="user")
public class User {
    @Id
    private Integer id;

    @Column(unique=true)
    private String username;

    private String password;
    private String passwordConfirm;

    private String rol;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getId() {
        return id;
    }

    public User() {
        rol = "USER";
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Transient
    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }
}
