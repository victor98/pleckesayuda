package com.ucbcba.edu.pleckes.Entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Comment {
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String text;

    @ManyToOne
    @JoinColumn(name="userId")
     private User user;
    @ManyToOne
     @JoinColumn(name="postId")
     private  Post post;

    private Integer likes=0;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
    public User getUser(){
        return user;
    }
   public  void setUser(User user){
        this.user=user;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }
}
