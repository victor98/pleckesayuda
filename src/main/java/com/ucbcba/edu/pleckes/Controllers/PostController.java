package com.ucbcba.edu.pleckes.Controllers;

import com.ucbcba.edu.pleckes.Entities.Category;
import com.ucbcba.edu.pleckes.Entities.Post;
import com.ucbcba.edu.pleckes.Entities.User;
import com.ucbcba.edu.pleckes.Services.CommentService;
import com.ucbcba.edu.pleckes.Services.PostService;
import com.ucbcba.edu.pleckes.Services.CategoryService;
import com.ucbcba.edu.pleckes.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class PostController {
    UserService userService;
    PostService postService;
    CategoryService categoryService;
    CommentService commentService;

    @Autowired
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    @Autowired
    public void setUserService(UserService userService) { this.userService = userService; }

    @Autowired
    private void setPostService(PostService postService){
        this.postService = postService;
    }

    @Autowired
    private void setCategoryService(CategoryService categoryService){
        this.categoryService = categoryService;
    }

    @RequestMapping(value="/post/editPost/{id}",method = RequestMethod.GET)
    public String edit (@PathVariable Integer id, Model model){
        Post post = postService.findPost(id);
        model.addAttribute("post",post);
        model.addAttribute("category",categoryService.listAllCategories());
        return "post/editPost";
    }

    @RequestMapping (value="/post/{id}")
    public String show (@PathVariable Integer id,Model model){
        Post post=postService.findPost(id);
        //String categoryName = post.getCategory().getName();
        model.addAttribute("commentList",commentService.listAllComments());
        model.addAttribute("post",post);
        return"post/showPost";
    }

    @RequestMapping (value = "/post/newPost/{id}", method = RequestMethod.GET)
    public String newPost(@PathVariable Integer id,Model model)
    {
        User user = userService.findById(id);
        model.addAttribute("user1",user);
        model.addAttribute("post",new Post());
        model.addAttribute("categories", categoryService.listAllCategories());
        return "post/newPost";
    }

    @RequestMapping (value = "/post",method = RequestMethod.POST)
    public String create (@ModelAttribute ("post") Post post, Model model)
    {
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = user.getUsername();
        com.ucbcba.edu.pleckes.Entities.User user1= userService.findByUsername(username);
        post.setUser(user1);
        postService.save(post);
        return "redirect:/welcome";
    }

    @RequestMapping(value = "/post/deletePost/{id}",method = RequestMethod.GET )
    public String delete(@PathVariable Integer id,Model model)
    {
        postService.deleteById(id);
        return "redirect:/welcome";
    }
    @RequestMapping(value = "/post/like/{id}", method = RequestMethod.GET)
    @ResponseBody
    public String like(@PathVariable Integer id, Model model) {
        Post post = postService.findPost(id);
        post.setLikes(post.getLikes()+1);
        postService.save(post);
        return post.getLikes().toString();
    }

    @RequestMapping(value = "/post/unlike/{id}", method = RequestMethod.GET)
    @ResponseBody
    public String unlike(@PathVariable Integer id, Model model) {
        Post post = postService.findPost(id);
        post.setLikes(post.getLikes()-1);
        postService.save(post);
        return post.getLikes().toString();
    }
    @RequestMapping(value="/post/search",method = RequestMethod.GET)
    public String search (@RequestParam("query")String query,Model model){
        List<Post> post=(List<Post>) postService.findByTitle(query);
        model.addAttribute("welcome",post);
        return "welcome";
    }
    @RequestMapping(value = "post/searchA/",method = RequestMethod.GET)
    public String searchA(@RequestParam("query") String query,Model model){
        List<Post> post=(List<Post>) postService.findByTitle(query);
        model.addAttribute("welcome",post);
        return "postTable";
    }
}