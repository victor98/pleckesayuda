package com.ucbcba.edu.pleckes.Controllers;

import com.ucbcba.edu.pleckes.Entities.Comment;
import com.ucbcba.edu.pleckes.Entities.Post;
import com.ucbcba.edu.pleckes.Entities.User;
import com.ucbcba.edu.pleckes.Services.CommentService;
import com.ucbcba.edu.pleckes.Services.PostService;
import com.ucbcba.edu.pleckes.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class ProfileController {
    UserService userService;
    PostService postService;
    CommentService commentService;

    @Autowired
    public void setUserService(UserService userService){this.userService = userService;}

    @Autowired
    public void setPostService(PostService postService){this.postService=postService;}

    @Autowired
    public void setCommentService (CommentService commentService) {
        this.commentService = commentService;
    }


    @RequestMapping(value = "/profile/{id}")
    public String perfil(@PathVariable Integer id,Model model) {
        Integer cantLikesPosts=0;
        Integer cantLikesComments=0;
        User user = userService.findById(id);
        List<Comment> comment= (List) commentService.listAllComments();
        List<Post> post = (List) postService.listAllPosts();

        for (Post p: post) {
            if(id==p.getUser().getId()){
                cantLikesPosts=cantLikesPosts+p.getLikes();
                for (Comment c:comment) {
                    if(p.getId()==c.getPost().getId()){
                        cantLikesComments=cantLikesComments+c.getLikes();
                    }
                }
            }
        }
        model.addAttribute("post", post);
        model.addAttribute("user",user);
        model.addAttribute("likesposts",cantLikesPosts);
        model.addAttribute("likescomments",cantLikesComments);

        model.addAttribute("comment",comment);
        return "profile";
    }

}