package com.ucbcba.edu.pleckes.Controllers;

import com.sun.org.apache.xpath.internal.operations.Mod;
import com.ucbcba.edu.pleckes.Entities.Category;
import com.ucbcba.edu.pleckes.Entities.Comment;
import com.ucbcba.edu.pleckes.Entities.Post;
import com.ucbcba.edu.pleckes.Entities.User;
import com.ucbcba.edu.pleckes.Repositories.CommentRepository;
import com.ucbcba.edu.pleckes.Services.CommentService;
import com.ucbcba.edu.pleckes.Services.PostService;
import com.ucbcba.edu.pleckes.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class CommentController {
    CommentService commentService;
    UserService userService;
    PostService postService;
    //CommentRepository commentRepository;
    @Autowired
    public void setCommentService (CommentService commentService) {
        this.commentService = commentService;
    }

    @Autowired
    public void setPostService(PostService postService) {
        this.postService = postService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/comment/editComment/{id}",method = RequestMethod.GET)
    public String edit(@PathVariable Integer id, Model model){
        Comment comment= commentService.findComment(id);
        model.addAttribute("comment",comment);
        return"comment/editComment";
    }
    @RequestMapping(value = "/comment/showComment/{id}")
    public String show(@PathVariable Integer id , Model model){
        Comment comment= commentService.findComment(id);
        model.addAttribute("comment",comment);
        return "comment/showComment";
    }
    @RequestMapping(value = "/comment/newComment/{id}")
    public String newComment(@PathVariable Integer id,Model model){
        Post post = postService.findPost(id);
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = user.getUsername();

        com.ucbcba.edu.pleckes.Entities.User user1= userService.findByUsername(username);
        Comment comment = new Comment();
        comment.setUser(user1);
        comment.setPost(post);
        model.addAttribute("user",user1);
        model.addAttribute("comment", comment);
        return "comment/newComment";
    }
    @RequestMapping(value = "/comment",method = RequestMethod.POST)
    public  String create(@ModelAttribute("comment")Comment comment,Model model){
        commentService.save(comment);
        return "redirect:/post/"+comment.getPost().getId().toString();
    }
    @RequestMapping(value="/comment/deleteComment/{id}",method = RequestMethod.GET)
    public String delete(@PathVariable Integer id,Model model){
        Integer idp = commentService.findComment(id).getPost().getId();
        commentService.deleteCommentById(id);
        return"redirect:/post/"+idp.toString();
    }

    @RequestMapping(value = "/comment/like/{id}", method = RequestMethod.GET)
    @ResponseBody
    public String like(@PathVariable Integer id, Model model) {
        Comment comment = commentService.findComment(id);
        comment.setLikes(comment.getLikes()+1);
        commentService.save(comment);
        return comment.getLikes().toString();
    }

    @RequestMapping(value = "/comment/unlike/{id}", method = RequestMethod.GET)
    @ResponseBody
    public String unlike(@PathVariable Integer id, Model model) {
        Comment comment = commentService.findComment(id);
        comment.setLikes(comment.getLikes()-1);
        commentService.save(comment);
        return comment.getLikes().toString();
    }
}
