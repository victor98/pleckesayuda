package com.ucbcba.edu.pleckes.Controllers;


import com.ucbcba.edu.pleckes.Entities.Post;
import com.ucbcba.edu.pleckes.Services.PostService;
import com.ucbcba.edu.pleckes.Services.UserService;
import javafx.geometry.Pos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {
    @Autowired
    UserService userService;
    @Autowired
    PostService postService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String registrationInit() {
        return "home";
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String admin() {
        return "admin";
    }

    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public String welcome(Model model) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = user.getUsername();

        com.ucbcba.edu.pleckes.Entities.User user1= userService.findByUsername(username);
        model.addAttribute("user", user1);

        ArrayList<Post> posts = (ArrayList) postService.listAllPosts();
        for (int i = 0; i < posts.size()-1; i++) {
            for (int j = 0; j < posts.size() - i - 1; j++) {
                if (posts.get(j).getLikes() < posts.get(j + 1).getLikes()) {
                    Post post = posts.get(j);
                    posts.set(j,posts.get(j+1));
                    posts.set(j+1, post);
                }
            }
        }
        model.addAttribute("posts", posts);

        return "welcome";
    }
}
