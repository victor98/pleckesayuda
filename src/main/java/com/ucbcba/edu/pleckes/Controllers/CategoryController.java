package com.ucbcba.edu.pleckes.Controllers;

import com.ucbcba.edu.pleckes.Entities.Category;
import com.ucbcba.edu.pleckes.Entities.Comment;
import com.ucbcba.edu.pleckes.Services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;

@Controller
public class CategoryController {
    CategoryService categoryService;


    @Autowired
    public void setCategoryService (CategoryService categoryService){
        this.categoryService = categoryService;
    }


    @RequestMapping(value = "/category", method = RequestMethod.GET)
    public String index(Model model) {
        List<Category> category = (List) categoryService.listAllCategories();
        model.addAttribute("category", category);
        return "category";
    }

    @RequestMapping(value="/category/edit/{id}",method = RequestMethod.GET)
    public String edit (@PathVariable Integer id, Model model){
        Category category = categoryService.findCategory(id);
        model.addAttribute("category",category);
        return "editCategory";
    }

    @RequestMapping (value="/category/{id}")
    public String show (@PathVariable Integer id,Model model){
        Category category= categoryService.findCategory(id);
        model.addAttribute("category",category);
        return"showCategory";
    }

    @RequestMapping (value = "/category/newCategory")
    public String newCategory(Model model)
    {
        model.addAttribute("category",new Category());
        return "newCategory";
    }

    @RequestMapping (value = "/category", method = RequestMethod.POST)
    public String create(@ModelAttribute("category") Category category, Model model)
    {
        categoryService.saveCategory(category);
        return "redirect:/category";
    }

    @RequestMapping(value = "/category/delete/{id}",method = RequestMethod.GET )
    public String delete (@PathVariable Integer id,Model model)
    {
        categoryService.deleteCategory(id);
        return "redirect:/category";
    }

}