package com.ucbcba.edu.pleckes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PleckesApplication {

	public static void main(String[] args) {
		SpringApplication.run(PleckesApplication.class, args);
	}
}
